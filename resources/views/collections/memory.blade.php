@extends('base')

@section('content')


<style>

.image {
    cursor: pointer;
}

.image:after {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background: #232323;
}

.image.open:after,
.image.solved:after {
    content: none;
}

</style>

<main>

    <div class="text-center p-10">

    @foreach($images as $image)

        <div id="image-{{ $image->id }}" class="inline-block relative bg-cover bg-center image image-{{ $image->id }}" style="height: 300px; width: 300px; background-image: url('https://sipi.participatory-archives.ch/{{$image->base_path != '' ? $image->base_path.'/' : ''}}{{$image->signature}}.jp2/full/320,/0/default.jpg')">
        </div>

    @endforeach

    </div>

</main>

<div class="overlay hidden fixed top-0 left-0 w-full h-full bg-red-500 opacity-10"></div>

<script>

let images = document.querySelectorAll('.image'),
    turn_count = 0;
    card_1 = undefined, card_2 = undefined;

for(let i = 0; i < images.length; i++){
    let image = images[i];

    image.addEventListener('click', (evt) => {
        let target = evt.currentTarget;

        if(!target.classList.contains('solved') || !target.classList.contains('open')) {

            evt.currentTarget.classList.add('open');

            if(card_1 != undefined) {
                turn_count++;

                card_2 = target;

                if(card_1.id == card_2.id) {
                    card_1.classList.remove('open');
                    card_2.classList.remove('open');

                    card_1.classList.add('solved');
                    card_2.classList.add('solved');

                    card_1 = undefined;
                    card_2 = undefined;

                    if(document.querySelectorAll('.image').length == document.querySelectorAll('.image.solved').length) {
                        alert('Yay, du häsches gschafft … Gnueg Arbeit für hüt, ab is Bier 🥳 🍻 Hesch nur '+turn_count+' Züg brucht.');
                    }
                } else {
                    document.querySelector('.overlay').classList.remove('hidden');
                    setTimeout(() => {
                        card_1.classList.remove('open');
                        card_2.classList.remove('open');

                        card_1 = undefined;
                        card_2 = undefined;

                        document.querySelector('.overlay').classList.add('hidden');
                    }, 2000);
                }
            } else {
                card_1 = target;
            }
        }
    })
}

</script>

@endsection
