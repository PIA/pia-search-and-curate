@extends('base')

@section('content')

<div class="p-4 md:p-14 bg-gray-100 min-h-screen">
    <div class="w-full text-center">
        <h1 class="text-4xl font-bold mb-4 text-red-500">Seite nicht gefunden…</h1>
        <p class="text-sm mb-4">…sie ist uns entwischt</p>
        <a href="{{ route('images.show', ['image' => 57131]) }}">
            <img class="m-auto" src="https://sipi.participatory-archives.ch/SGV_12/SGV_12N_45100.jp2/full/640,/0/default.jpg" alt="Katze beim Mausen">
        </a>
    </div>
    <div class="w-full mt-8">
        <h2 class="text-xl font-bold mb-4">Zufälligkeiten</h2>

        <p class="text-sm">Anstatt der gewünschten Seite präsentieren wir Ihnen hier zufälliges Material aus unseren Sammlungen. </p>

    </div>
    <div class="flex my-8">
        <div class="w-1/2">
            <h3 class="font-bold mb-2">Zufällige Collections</h3>
            @foreach($collections as $collection)
                <x-links.default href="{{ route('collections.show', [$collection]) }}" :label="$collection->label" class="mb-2"/>
            @endforeach

            <h3 class="font-bold mb-2 mt-4">Zufällige Schlagworte</h3>
            @foreach($keywords as $keyword)
                @if ($keyword->label)
                    <x-links.default href="/?keyword={{ $keyword->id }}" :label="$keyword->label" class="mb-2"/>
                @endif
            @endforeach

             <h3 class="font-bold mb-2 mt-4">Zufälliges Bild</h3>
            <p class="text-xs mb-2">Es haben nicht alle Objekte ein Bild hinterlegt. Darum ist der folgende Block manchmal leer.</p>
            <figure>
                <a href="{{ route('images.show', [$image]) }}">
                    <img src="https://sipi.participatory-archives.ch/{{$image->base_path != '' ? $image->base_path.'/' : ''}}{{$image->signature}}.jp2/full/560,/0/default.jpg" alt="{{ $image->title }}">
                </a>
                <figcaption class="text-sm text-italic">{{ $image->title }}</figcaption>
            </figure>

        </div>
        <div class="w-1/2">

            <h2 class="font-bold mb-2 mt-4">Weiterführende Links</h2>
            <ul class="list">
                <li>&mdash; <a href="/">Startseite</a></li>
                <li>&mdash; <a href="{{ route('collections.index') }}">Übersicht der Collections</a></li>
                <li>&mdash; <a href="{{ route('keywords.index') }}">Übersicht der Schlagworte</a></li>
        </ul>

        </div>
    </div>
</div>

@endsection
